# README #

This repository contains the most fundamental examples of Nauticle to present the applicability of the implemented numerical schemes. To obtain the examples in a single package, please visit the Downloads section.

The examples.zip holds the files required by the simulations as well as the simulation result files. They can be read and visualized in Paraview with the recommended version of 5.2.0.

For further information about the examples please read the User's Guide of Nauticle.